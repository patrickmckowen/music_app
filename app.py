from flask import Flask, render_template, request, session
from werkzeug import secure_filename
import os
from functions import *

#%%
conn = sqlite3.connect('audio.db')
c = conn.cursor()
# ------------------------------------------------------------------------ #

UPLOAD_FOLDER = '/uploads'
ALLOWED_EXTENSIONS = set(['wav', 'WAV', 'pgm', 'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)



app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER



@app.route('/')
def index():
       return render_template('index.html')



@app.route('/upload_file')
def upload_file():
       return render_template('upload_file.html')



@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
    if 'username' in session:         # If the user is logged in
        if request.method == 'POST':  # If the method was post
            f = request.files['file'] # Assign variable to incoming file
            descriptors = escape_sql(request.form.getlist('descriptors[]'))
            # FUNCTION
            ## - filename
            ## - username
            ## - datetime
            ## - descriptors (Add functions to escape sql for security)
            handle_single_file_upload(f.filename,
                                      session['user'],
                                      get_date(),
                                      descriptors)
            f.save(os.path.join('uploads', secure_filename(f.filename)))
            return 'file uploaded successfully'
    else:

if __name__ == '__main__':
    app.run(debug = True)
