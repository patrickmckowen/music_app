from flask import Flask, render_template, request
from werkzeug import secure_filename
import os, sqlite3


#%% IMPORTS
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import datetime
from sqlalchemy import (Table, Column, Integer, Numeric, String, DateTime,
ForeignKey, Boolean)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

import os
import sys

#%%

engine = create_engine('sqlite:///Files.db')
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()

#%% File CLASS

class File(Base):
    '''
    
    '''
    root_dir = r"C:\Users\Patrick\AppData\Local\Continuum\Anaconda3\Lib\site-packages\mined_minds\File" # Need to make it a reference to the path of __file__ ...
    __tablename__ = 'files'
    file_id = Column(Integer, primary_key=True)
    title = Column(String(50), index=True)
    description = Column(String(500))
    insert_date = Column(DateTime(), default=datetime.now)
    
    def __init__(self, title, body, dir_name, description):
        self.title = title
        self.body = body
        self.dir_name = dir_name
        self.discription = description
    
# ------------------------------------------------------------------------ #

UPLOAD_FOLDER = '/uploads'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/')
def index():
       return render_template('index.html')

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        f.save(os.path.join('uploads', secure_filename(f.filename)))
        return 'file uploaded successfully'

if __name__ == '__main__':
    app.run(debug = True)
